<?php
/**
 * User: Jos
 * Date: 23-11-2015
 */

namespace TheNextSoftware\CoreBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Router;
use TheNextSoftware\CoreBundle\Controller\BaseCompanyController;

class CompanyListener
{
	/** @var  Router */
	private $router;

	/** @var  EntityManager */
	private $em;

	public function __construct(Router $router, EntityManager $em)
	{
		$this->router = $router;
		$this->em = $em;
	}

	public function onKernelController(FilterControllerEvent $event)
	{
		$controller = $event->getController();

		/*
		 * $controller passed can be either a class or a Closure.
		 * This is not usual in Symfony but it may happen.
		 * If it is a class, it comes in array format
		 */
		if (!is_array($controller)) {
			return;
		}

		if ($controller[0] instanceof BaseCompanyController) {
			$company_id = $event->getRequest()->attributes->get("company_id");
			$controller[0]->setCompany($company_id);

			if(!in_array($event->getRequest()->get('_route'), [ "company_create", "company_upgrade", "company_activate", "company_start_subscription", "company_finish_subscription" ]))
			{
				$company = $controller[0]->getCompany();
				$subscription = $company->getSubscription();

				if($company->getPackage() == NULL
					&& $this->em->getRepository("TheNextCoreBundle:Package")->findOneBy([]))
				{
					throw new HttpException(307, null, null, [
						'Location' => $this->router->generate('company_activate', [
							'company_id' => $company_id
						])
					]);
				}else if(($subscription == NULL || !$subscription->isValid()) && $company->getPackage()->getPrice() > 0)
				{
					throw new HttpException(307, null, null, [
						'Location' => $this->router->generate('company_start_subscription', [
							'company_id' => $company_id
						])
					]);
				}
			}
		}
	}
}