<?php

namespace TheNextSoftware\CoreBundle\Service;

use TheNextSoftware\CoreBundle\Entity\User;

class UserService {
  private $userPhotoUrlBase;

  public function __construct($userPhotoUrlBase) {
    $this->userPhotoUrlBase = $userPhotoUrlBase;
  }

  public function userWithPhotoUrl(User $user) {
    $user->setPhotoUrlBase($this->userPhotoUrlBase);
    return $user;
  }
}