<?php
/**
 * User: Jos Craaijo
 * Date: 12-7-2016
 */

namespace TheNextSoftware\CoreBundle\Service;


class PackageFeatureManager
{
	private $features;

	public function setFeatures($features)
	{
		$this->features = $features;
	}

	public function getFeatures()
	{
		return $this->features;
	}
}