<?php
/**
 * User: Jos Craaijo
 * Date: 6-7-2016
 */

namespace TheNextSoftware\CoreBundle\Service;

use AppBundle\Service\Mailer;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use TheNextSoftware\CoreBundle\Entity\Company;
use TheNextSoftware\CoreBundle\Entity\User;

class NewUserService
{
	/** @var  UserPasswordEncoder */
	private $passwordEncoder;

	/** @var  \Twig_Environment */
	private $twig;

	/** @var  EntityManager */
	private $manager;
    private $mailer;

    public function __construct(UserPasswordEncoder $passwordEncoder, Mailer $mailer, \Twig_Environment $twig, EntityManager $manager)
	{
		$this->passwordEncoder = $passwordEncoder;
		$this->twig = $twig;
		$this->manager = $manager;
        $this->mailer = $mailer;
	}

	public function addNewAccount(User $user)
	{
		$isNewAccount = $user->getPlainPassword() == null;
		if($isNewAccount)
		{
			$code = str_replace(['+', '/'], ['-', '_'], base64_encode(random_bytes(96)));
			$user->setActivationCode($code);
			$user->setPlainPassword(substr($code, 0, 50)); // Nodig omdat het wachtwoord niet NULL mag zijn
			// TODO: Sta wachtwoord op NULL toe, met als resultaat dat je niet in kan loggen.
		}

		$this->updatePassword($user);

		$this->manager->persist($user);
		$this->manager->flush();

		if($isNewAccount)
		{
			$this->mailNewAccountWithActivation($user);
		}else{
			$this->mailNewAccountWithPassword($user);
		}
	}

	public function mailNewAccountWithActivation(User $user)
	{
        $this->mailer->sendMail("Welkom bij {$this->mailer->getSiteName()}", $this->twig->render("@TheNextCore/mail/newAccountActivation.html.twig", [
            "user" => $user
        ]), [ $user->getEmail() ]);
	}

	public function resetPassword(User $user)
	{
		$code = str_replace(['+', '/'], ['-', '_'], base64_encode(random_bytes(96)));
		$user->setActivationCode($code);

		$this->manager->persist($user);
		$this->manager->flush();

        $this->mailer->sendMail("Wachtwoord resetten", $this->twig->render("@TheNextCore/mail/passwordReset.html.twig", [
            "user" => $user
        ]), [ $user->getEmail() ]);
	}

	public function mailAddedToRestaurant(User $user, Company $company)
	{
        $this->mailer->sendMail("Welkom bij {$this->mailer->getSiteName()}", $this->twig->render("@TheNextCore/mail/employeeAdded.html.twig", [
            "user" => $user,
            "company" => $company
        ]), [ $user->getEmail() ]);
	}

	public function mailNewAccountWithPassword(User $user)
    {
        $this->mailer->sendMail("Welkom bij {$this->mailer->getSiteName()}", $this->twig->render("@TheNextCore/mail/newAccount.html.twig", [
            "user" => $user
        ]), [ $user->getEmail() ]);
	}

	public function updatePassword(User $user)
	{
		$password = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
		$user->setPassword($password);
	}
}