<?php

namespace TheNextSoftware\CoreBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use TheNextSoftware\CoreBundle\Type\EnumSubscriptionStatus;

/**
 * Subscription
 */
class Subscription
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $debcode;

    /**
     * @var \DateTime
     */
    private $startedOn;

    /**
     * @var \DateTime
     */
    private $cancelledPer;

    /**
     * @var string
     */
    private $brqInvoiceNumber;

    /**
     * @var string
     */
    private $brqConsumerBic;

    /**
     * @var string
     */
    private $brqConsumerIban;

    /**
     * @var string
     */
    private $brqConsumerIssuer;

    /**
     * @var string
     */
    private $brqConsumerName;

    /**
     * @var string
     */
    private $brqTimestamp;

    /**
     * @var string
     */
    private $brqTransactions;

    /**
     * @var integer
     */
    private $tniId;

    public function __construct()
    {
        $this->setStartedOn(new \DateTime());
        $this->transactions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Subscription
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set debcode
     *
     * @param string $debcode
     *
     * @return Subscription
     */
    public function setDebcode($debcode)
    {
        $this->debcode = $debcode;

        return $this;
    }

    /**
     * Get debcode
     *
     * @return string
     */
    public function getDebcode()
    {
        return $this->debcode;
    }

    /**
     * Set startedOn
     *
     * @param \DateTime $startedOn
     *
     * @return Subscription
     */
    public function setStartedOn($startedOn)
    {
        $this->startedOn = $startedOn;

        return $this;
    }

    /**
     * Get startedOn
     *
     * @return \DateTime
     */
    public function getStartedOn()
    {
        return $this->startedOn;
    }

    /**
     * Set cancelledPer
     *
     * @param \DateTime $cancelledPer
     *
     * @return Subscription
     */
    public function setCancelledPer($cancelledPer)
    {
        $this->cancelledPer = $cancelledPer;

        return $this;
    }

    /**
     * Get cancelledPer
     *
     * @return \DateTime
     */
    public function getCancelledPer()
    {
        return $this->cancelledPer;
    }

    /**
     * Set brqInvoiceNumber
     *
     * @param string $brqInvoiceNumber
     *
     * @return Subscription
     */
    public function setBrqInvoiceNumber($brqInvoiceNumber)
    {
        $this->brqInvoiceNumber = $brqInvoiceNumber;

        return $this;
    }

    /**
     * Get brqInvoiceNumber
     *
     * @return string
     */
    public function getBrqInvoiceNumber()
    {
        return $this->brqInvoiceNumber;
    }

    /**
     * Set brqConsumerBic
     *
     * @param string $brqConsumerBic
     *
     * @return Subscription
     */
    public function setBrqConsumerBic($brqConsumerBic)
    {
        $this->brqConsumerBic = $brqConsumerBic;

        return $this;
    }

    /**
     * Get brqConsumerBic
     *
     * @return string
     */
    public function getBrqConsumerBic()
    {
        return $this->brqConsumerBic;
    }

    /**
     * Set brqConsumerIban
     *
     * @param string $brqConsumerIban
     *
     * @return Subscription
     */
    public function setBrqConsumerIban($brqConsumerIban)
    {
        $this->brqConsumerIban = $brqConsumerIban;

        return $this;
    }

    /**
     * Get brqConsumerIban
     *
     * @return string
     */
    public function getBrqConsumerIban()
    {
        return $this->brqConsumerIban;
    }

    /**
     * Set brqConsumerIssuer
     *
     * @param string $brqConsumerIssuer
     *
     * @return Subscription
     */
    public function setBrqConsumerIssuer($brqConsumerIssuer)
    {
        $this->brqConsumerIssuer = $brqConsumerIssuer;

        return $this;
    }

    /**
     * Get brqConsumerIssuer
     *
     * @return string
     */
    public function getBrqConsumerIssuer()
    {
        return $this->brqConsumerIssuer;
    }

    /**
     * Set brqConsumerName
     *
     * @param string $brqConsumerName
     *
     * @return Subscription
     */
    public function setBrqConsumerName($brqConsumerName)
    {
        $this->brqConsumerName = $brqConsumerName;

        return $this;
    }

    /**
     * Get brqConsumerName
     *
     * @return string
     */
    public function getBrqConsumerName()
    {
        return $this->brqConsumerName;
    }

    /**
     * Set brqTimestamp
     *
     * @param string $brqTimestamp
     *
     * @return Subscription
     */
    public function setBrqTimestamp($brqTimestamp)
    {
        $this->brqTimestamp = $brqTimestamp;

        return $this;
    }

    /**
     * Get brqTimestamp
     *
     * @return string
     */
    public function getBrqTimestamp()
    {
        return $this->brqTimestamp;
    }

    /**
     * Set brqTransactions
     *
     * @param string $brqTransactions
     *
     * @return Subscription
     */
    public function setBrqTransactions($brqTransactions)
    {
        $this->brqTransactions = $brqTransactions;

        return $this;
    }

    /**
     * Get brqTransactions
     *
     * @return string
     */
    public function getBrqTransactions()
    {
        return $this->brqTransactions;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $transactions;


    /**
     * Add transaction
     *
     * @param Transaction $transaction
     *
     * @return Subscription
     */
    public function addTransaction(Transaction $transaction)
    {
        $this->transactions[] = $transaction;

        return $this;
    }

    /**
     * Remove transaction
     *
     * @param Transaction $transaction
     */
    public function removeTransaction(Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }
    /**
     * @var Company
     */
    private $company;


    /**
     * Set restaurant
     *
     * @param Company $company
     *
     * @return Subscription
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    public function isValid()
	{
		return in_array($this->getStatus(), [ EnumSubscriptionStatus::STATUS_ACTIVE, EnumSubscriptionStatus::STATUS_DEMO ]);
	}

    /**
     * Get restaurant
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }
    /**
     * @var string
     */
    private $stripeCustomerId;

    /**
     * @var string
     */
    private $stripeSubscriptionId;


    /**
     * Set stripeCustomerId
     *
     * @param string $stripeCustomerId
     *
     * @return Subscription
     */
    public function setStripeCustomerId($stripeCustomerId)
    {
        $this->stripeCustomerId = $stripeCustomerId;

        return $this;
    }

    /**
     * Get stripeCustomerId
     *
     * @return string
     */
    public function getStripeCustomerId()
    {
        return $this->stripeCustomerId;
    }

    /**
     * Set stripeSubscriptionId
     *
     * @param string $stripeSubscriptionId
     *
     * @return Subscription
     */
    public function setStripeSubscriptionId($stripeSubscriptionId)
    {
        $this->stripeSubscriptionId = $stripeSubscriptionId;

        return $this;
    }

    /**
     * Get stripeSubscriptionId
     *
     * @return string
     */
    public function getStripeSubscriptionId()
    {
        return $this->stripeSubscriptionId;
    }

    /**
     * Set tniId
     *
     * @param string $tniId
     *
     * @return Subscription
     */
    public function setTniId($tniId)
    {
        $this->tniId = $tniId;

        return $this;
    }

    /**
     * Get tniId
     *
     * @return integer
     */
    public function getTniId()
    {
        return $this->tniId;
    }
}
