<?php
/**
 * User: Jos Craaijo
 * Date: 11-7-2016
 */

namespace Entity;


interface CompanyDataInterface
{
	public function getCompany();
}