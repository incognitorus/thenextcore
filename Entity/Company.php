<?php

namespace TheNextSoftware\CoreBundle\Entity;
use AppBundle\Entity\CompanyData;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;


/**
 * Restaurant
 * @Assert\Callback(callback="validateIban")
 */
class Company
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $zipcode;

    /**
     * @var string
     */
    private $kvkNumber;

    /**
     * @var string
     */
    private $ibanNumber;

    /**
     * @var User
     */
    private $owner;

    /**
     * @var ArrayCollection
     */
    private $surveys;

    /**
     * @var PromocodeActivation
     */
    private $promocode;

    /**
     * @var \DateTime
     */
    private $packageSetOn;

    /**
     * @var Package
     */
    private $package;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $employees;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->surveys = new ArrayCollection();
        $this->mailRecipients = new ArrayCollection();
        $this->packageSetOn = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get ownerId
     *
     * @return int
     */
    public function getOwnerId()
    {
        return $this->getOwner()->getId();
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Company
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Company
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return Company
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set kvkNumber
     *
     * @param string $kvkNumber
     *
     * @return Company
     */
    public function setKvkNumber($kvkNumber)
    {
        $this->kvkNumber = $kvkNumber;

        return $this;
    }

    /**
     * Get kvkNumber
     *
     * @return string
     */
    public function getKvkNumber()
    {
        return $this->kvkNumber;
    }

    /**
     * Set ibanNumber
     *
     * @param string $ibanNumber
     *
     * @return Company
     */
    public function setIbanNumber($ibanNumber)
    {
        $this->ibanNumber = $ibanNumber;

        return $this;
    }

    /**
     * Get ibanNumber
     *
     * @return string
     */
    public function getIbanNumber()
    {
        return $this->ibanNumber;
    }

    /**
     * Set owner
     *
     * @param User $owner
     *
     * @return Company
     */
    public function setOwner(User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }


    /**
     * Set promocode
     *
     * @param PromocodeActivation $promocode
     *
     * @return Company
     */
    public function setPromocode(PromocodeActivation $promocode = null)
    {
        $this->promocode = $promocode;

        return $this;
    }

    /**
     * Get promocode
     *
     * @return PromocodeActivation
     */
    public function getPromocode()
    {
        return $this->promocode;
    }


    /**
     * Set packageSetOn
     *
     * @param \DateTime $packageSetOn
     *
     * @return Company
     */
    public function setPackageSetOn($packageSetOn)
    {
        $this->packageSetOn = $packageSetOn;

        return $this;
    }

    /**
     * Get packageSetOn
     *
     * @return \DateTime
     */
    public function getPackageSetOn()
    {
        return $this->packageSetOn;
    }

    /**
     * Set package
     *
     * @param Package $package
     *
     * @return Company
     */
    public function setPackage(Package $package = null)
    {
        $this->package = $package;

        return $this;
    }

    /**
     * Get package
     *
     * @return Package
     */
    public function getPackage()
    {
        return $this->package;
    }
    /**
     * @var Subscription
     */
    private $subscription;


    /**
     * Set subscription
     *
     * @param Subscription $subscription
     *
     * @return Company
     */
    public function setSubscription(Subscription $subscription = null)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Get subscription
     *
     * @return Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * Add employee
     *
     * @param UserRole $employee
     *
     * @return Company
     */
    public function addEmployee(UserRole $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param UserRole $employee
     */
    public function removeEmployee(UserRole $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getEmployees()
    {
        return $this->employees;
    }
    /**
     * @var CompanyData
     */
    private $data;


    /**
     * Set data
     *
     * @param CompanyData $data
     *
     * @return Company
     */
    public function setData(CompanyData $data = null)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return CompanyData
     */
    public function getData()
    {
        return $this->data;
    }

    public function hasFeature($name)
    {
        if($this->getPackage() == null)
        {
            return false;
        }

        return $this->getPackage()->hasFeature($name);
    }
    /**
     * @var string
     */
    private $phoneNumber;


    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Company
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledCollectionDeletions() AS $col)
        {
            throw new \Exception("Don't ever delete Companies!");
        }
    }

    public function preRemove()
    {
        throw new \Exception("Don't ever delete Companies!");
    }

    public function validateIban(ExecutionContextInterface $context)
    {
        $subscription = $this->getSubscription();

        if ($subscription && $subscription->getStatus() === 'active' && !$this->getIbanNumber()) {
            $context->addViolation('IBAN number: This value should not be blan');
        }
    }
}
