<?php

namespace TheNextSoftware\CoreBundle\Entity;

/**
 * Factuur
 */
class Factuur
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $factuurnummer;

    /**
     * @var \DateTime
     */
    private $datum;

    /**
     * @var int
     */
    private $prijs;

    /**
     * @var int
     */
    private $korting;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set factuurnummer
     *
     * @param string $factuurnummer
     *
     * @return Factuur
     */
    public function setFactuurnummer($factuurnummer)
    {
        $this->factuurnummer = $factuurnummer;

        return $this;
    }

    /**
     * Get factuurnummer
     *
     * @return string
     */
    public function getFactuurnummer()
    {
        return $this->factuurnummer;
    }

    /**
     * Set datum
     *
     * @param \DateTime $datum
     *
     * @return Factuur
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;

        return $this;
    }

    /**
     * Get datum
     *
     * @return \DateTime
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * Set prijs
     *
     * @param integer $prijs
     *
     * @return Factuur
     */
    public function setPrijs($prijs)
    {
        $this->prijs = $prijs;

        return $this;
    }

    /**
     * Get prijs
     *
     * @return int
     */
    public function getPrijs()
    {
        return $this->prijs;
    }

    /**
     * Set korting
     *
     * @param integer $korting
     *
     * @return Factuur
     */
    public function setKorting($korting)
    {
        $this->korting = $korting;

        return $this;
    }

    /**
     * Get korting
     *
     * @return int
     */
    public function getKorting()
    {
        return $this->korting;
    }
}
