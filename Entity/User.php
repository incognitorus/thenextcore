<?php

namespace TheNextSoftware\CoreBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * User
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $ip;

    /**
     * @var string
     *
     */
    private $email;

    /**
     * @var \DateTime
     */
    private $createdOn;

    /**
     * @var \DateTime
     */
    private $lastLogin;

    /**
     * @var boolean
     */
    private $isAdmin = FALSE;

    /**
     * @var string
     */
    private $photoUrlBase = '';

    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return User
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     *
     * @return User
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set lastLogin
     *
     * @param \DateTime $lastLogin
     *
     * @return User
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        if($this->getIsAdmin())
        {
            return [ 'ROLE_USER', 'ROLE_ADMIN' ];
        }else{
            return [ 'ROLE_USER' ];
        }
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return NULL;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->createdOn,
            $this->lastLogin,
            $this->firstName,
            $this->lastName,
            $this->isAdmin
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->createdOn,
            $this->lastLogin,
            $this->firstName,
            $this->lastName,
            $this->isAdmin
            ) = unserialize($serialized);
    }

    /**
     * Get restaurants
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCompanies()
    {
        return new ArrayCollection(array_merge($this->getOwnedCompanies()->toArray(), $this->getCompanyRoles()->map(function ($role) {
            /** @var $role UserRole */
            return $role->getCompany();
        })->toArray()));
    }

    /**
     * Sets created_at to the current date.
     */
    public function updateCreatedOn()
    {
        $this->setCreatedOn(new \DateTime());
    }

    /**
     * Sets created_at to the current date.
     */
    public function updateLastLogin()
    {
        $this->setLastLogin(new \DateTime());
    }

    public function __toString()
    {
        return $this->getEmail();
    }


    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     *
     * @return User
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return boolean
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ownedCompanies;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $companyRoles;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userPhotos;


    /**
     * Add ownedCompany
     *
     * @param Company $ownedCompany
     *
     * @return User
     */
    public function addOwnedCompany(Company $ownedCompany)
    {
        $this->ownedCompanies[] = $ownedCompany;

        return $this;
    }

    /**
     * Remove ownedCompany
     *
     * @param Company $ownedCompany
     */
    public function removeOwnedCompany(Company $ownedCompany)
    {
        $this->ownedCompanies->removeElement($ownedCompany);
    }

    /**
     * Get ownedCompanies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOwnedCompanies()
    {
        return $this->ownedCompanies;
    }

    /**
     * Add companyRole
     *
     * @param UserRole $companyRole
     *
     * @return User
     */
    public function addCompanyRole(UserRole $companyRole)
    {
        $this->companyRoles[] = $companyRole;

        return $this;
    }

    /**
     * Remove companyRole
     *
     * @param UserRole $companyRole
     */
    public function removeCompanyRole(UserRole $companyRole)
    {
        $this->companyRoles->removeElement($companyRole);
    }

    /**
     * Get companyRoles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanyRoles()
    {
        return $this->companyRoles;
    }

    /**
     * Add userRole
     *
     * @param UserPhoto $userPhoto
     *
     * @return User
     */
    public function addUserPhoto(UserPhoto $userPhoto)
    {
        $this->userPhotos[] = $userPhotos;

        return $this;
    }

    /**
     * Remove userPhoto
     *
     * @param UserPhoto $userPhoto
     */
    public function removeUserPhoto(UserPhoto $userPhoto)
    {
        $this->userPhotos->removeElement($userPhoto);
    }

    /**
     * Get userPhotos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserPhotos()
    {
        return $this->userPhotos;
    }

    /**
     * Get userPhoto
     *
     * @return string
     */
    public function getPhoto()
    {
        if (count($this->userPhotos) > 0) {
            return $this->userPhotos->last();
        }

        return null;
    }



    /**
     * Get userPhotoUrl
     *
     * @return string
     */
    public function getPhotoUrl()
    {
        $lastPhoto = $this->getPhoto();

        if ($lastPhoto) {
            return "{$this->getPhotoUrlBase()}{$lastPhoto->getFilename()}";
        }

        return null;
    }

    /**
     * @var string
     */
    private $activationCode;


    /**
     * Set activationCode
     *
     * @param string $activationCode
     *
     * @return User
     */
    public function setActivationCode($activationCode)
    {
        $this->activationCode = $activationCode;

        return $this;
    }

    /**
     * Get activationCode
     *
     * @return string
     */
    public function getActivationCode()
    {
        return $this->activationCode;
    }


    /**
     * Set photoUrlBase
     *
     * @param string $photoUrlBase
     *
     * @return User
     */
    public function setPhotoUrlBase($photoUrlBase) {
        $this->photoUrlBase = $photoUrlBase;

        return $this;
    }

    /**
     * Get photoUrlBase
     *
     * @return string
     */
    public function getPhotoUrlBase()
    {
        return $this->photoUrlBase;
    }
}
