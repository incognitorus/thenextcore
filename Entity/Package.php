<?php

namespace TheNextSoftware\CoreBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Package
 */
class Package
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var boolean
     */
    private $isVisible;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Package
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Package
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set isVisible
     *
     * @param boolean $isVisible
     *
     * @return Package
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * Get isVisible
     *
     * @return boolean
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    public function __toString()
    {
        return $this->getName();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $features;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->features = new ArrayCollection();
    }

    /**
     * Add feature
     *
     * @param string $feature
     *
     * @return Package
     */
    public function addFeature($feature)
    {
        if($this->getFeatures()->contains($feature))
        {
            return $this;
        }

        $feature = new PackageFeature($feature);
        $feature->setPackage($this);
        $this->features[] = $feature;

        return $this;
    }

    /**
     * Remove feature
     *
     * @param string $feature
     * @return $this
     */
    public function removeFeature($feature)
    {
        $toRemove = $this->features->filter(function ($featureEntity) use($feature){
            /** @var $featureEntity PackageFeature */
            return $featureEntity->getName() == $feature;
        });

        foreach($toRemove as $entity)
        {
            $this->features->removeElement($entity);
        }

        return $this;
    }

    /**
     * Get features
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatures()
    {
        return $this->features->map(function ($feature) {
            /** @var $feature PackageFeature */
            return $feature->getName();
        });
    }

    public function hasFeature($name)
    {
        return in_array($name, $this->getFeatures()->toArray());
    }
}
