<?php

namespace TheNextSoftware\CoreBundle\Entity;

/**
 * TransactionData
 */
class TransactionData
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $data;

    /**
     * @var string
     */
    private $createdOn;


	public function __construct()
	{
		$this->createdOn = new \DateTime();
	}

	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return TransactionData
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set createdOn
     *
     * @param string $createdOn
     *
     * @return TransactionData
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return string
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
    /**
     * @var Transaction
     */
    private $transaction;


    /**
     * Set transaction
     *
     * @param Transaction $transaction
     *
     * @return TransactionData
     */
    public function setTransaction(Transaction $transaction)
    {
        $this->transaction = $transaction;

        return $this;
    }

    /**
     * Get transaction
     *
     * @return Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }
}
