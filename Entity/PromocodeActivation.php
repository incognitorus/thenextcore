<?php

namespace TheNextSoftware\CoreBundle\Entity;

/**
 * PromocodeActivation
 */
class PromocodeActivation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $activatedOn;

    /**
     * @var Promocode
     */
    private $promocode;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activatedOn
     *
     * @param \DateTime $activatedOn
     *
     * @return PromocodeActivation
     */
    public function setActivatedOn($activatedOn)
    {
        $this->activatedOn = $activatedOn;

        return $this;
    }

    /**
     * Get activatedOn
     *
     * @return \DateTime
     */
    public function getActivatedOn()
    {
        return $this->activatedOn;
    }

    /**
     * Set promocode
     *
     * @param Promocode $promocode
     *
     * @return PromocodeActivation
     */
    public function setPromocode(Promocode $promocode = null)
    {
        $this->promocode = $promocode;

        return $this;
    }

    /**
     * Get promocode
     *
     * @return Promocode
     */
    public function getPromocode()
    {
        return $this->promocode;
    }
}
