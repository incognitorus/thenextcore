<?php

namespace TheNextSoftware\CoreBundle\Entity;

/**
 * UserRole
 */
class UserRole
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    /**
     * @var boolean
     */
    private $manageAccess = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return UserRole
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set manageAccess
     *
     * @param boolean $manageAccess
     *
     * @return UserRole
     */
    public function setManageAccess($manageAccess)
    {
        $this->manageAccess = $manageAccess;

        return $this;
    }

    /**
     * Get manageAccess
     *
     * @return boolean
     */
    public function hasManageAccess()
    {
        return $this->manageAccess;
    }
    /**
     * @var Company
     */
    private $company;


    /**
     * Set company
     *
     * @param Company $company
     *
     * @return UserRole
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Get manageAccess
     *
     * @return boolean
     */
    public function getManageAccess()
    {
        return $this->manageAccess;
    }
}
