<?php
/**
 * User: Jos Craaijo
 * Date: 11-7-2016
 */

namespace TheNextSoftware\CoreBundle\Controller;


use Doctrine\Common\Collections\Collection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use TheNextSoftware\CoreBundle\Entity\Company;
use TheNextSoftware\CoreBundle\Entity\Promocode;
use TheNextSoftware\CoreBundle\Entity\PromocodeActivation;
use TheNextSoftware\CoreBundle\Form\Type\CompanyType;
use TheNextSoftware\CoreBundle\Form\Validation\PromocodeIsNotExpiredConstraint;

class CompanySelectController extends Controller
{
	public function indexAction()
	{
		/** @var Collection $companies */
		$companies = $this->getUser()->getCompanies();
		if($companies->count() == 1)
		{
			return $this->redirectToRoute("company_home", [ "company_id" => $companies[0]->getId() ]);
		}

		return $this->render('@TheNextCore/Company/choose.html.twig', [
			'companies' => $companies
		]);
	}

	public function createAction(Request $request)
	{
		$company = new Company();

		$form = $this->createFormBuilder([
		    'company' => $company,
            'promocode' => null,
        ])
            ->add('promocode', TextType::class, [
                'label' => 'Promocode',
                'required' => false,
                'constraints' => [
                    new PromocodeIsNotExpiredConstraint()
                ]
            ])
            ->add('company', CompanyType::class, [
                'label' => 'Bedrijf',
            ]);

        $promocodeRepo = $this->getDoctrine()->getRepository('TheNextCoreBundle:Promocode');
		$form
            ->get('promocode')
            ->addModelTransformer(new CallbackTransformer(
                function($entity)
                {
                    /** @var $entity Promocode */
                    return $entity == null ? null : $entity->getCode();
                },
                function ($promocode) use($promocodeRepo)
                {
                    return $promocodeRepo->findOneBy([
                        'code' => $promocode
                    ]);
                }
            ));

        $form = $form->getForm();

		$form->handleRequest($request);
		if($form->isValid() && $form->isSubmitted())
		{
			$company->setOwner($this->getUser());
			$pa = new PromocodeActivation();

			/** @var Promocode $promocode */
            $promocode = $form->getData()['promocode'];
            if($promocode != null)
            {
                $pa->setPromocode($promocode);
                $pa->setActivatedOn(new \DateTime());
                $company->setPromocode($pa);
            }

			$this->get('company_config')->applySiteSpecificOptions($company);

			$em = $this->getDoctrine()->getManager();
			$em->persist($company);
			$em->flush();

			return $this->redirectToRoute('company_create_success', [ 'company_id' => $company->getId() ]);
		}

		return $this->render(
			'@TheNextCore/Company/create.html.twig',
			[
				'form' => $form->createView()
			]
		);
	}
}