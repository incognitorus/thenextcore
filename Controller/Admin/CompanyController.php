<?php
/**
 * User: Jos Craaijo
 * Date: 11-7-2016
 */

namespace TheNextSoftware\CoreBundle\Controller\Admin;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use TheNextSoftware\CoreBundle\Entity\Company;
use TheNextSoftware\CoreBundle\Form\Type\Admin\AdminCompanyType;

class CompanyController extends BaseAdminController
{
	public function __construct()
	{
		$this->showRoute = 'admin_company_show';
		$this->listRoute = 'admin_company_list';
		$this->editRoute = 'admin_company_edit';
		$this->createRoute = 'admin_company_create';
	}

	public function indexAction()
	{
		return $this->render('TheNextCoreBundle:Admin/Company:index.html.twig', [
			'entities' => $this->getAllFromRepo('TheNextCoreBundle:Company'),
			'create_route' => $this->createRoute,
			'show_route' => $this->showRoute
		]);
	}

	/**
	 * @ParamConverter("entity")
	 */
	public function showAction(Company $entity, Request $request)
	{
		$jwtManager = $this->container->get('lexik_jwt_authentication.jwt_manager');
		$user = $entity->getOwner();
		$token = $jwtManager->create($user);

		$form = $this->createDeleteForm($entity);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid() && $request->getMethod() == 'DELETE') {
			$em = $this->getDoctrine()->getManager();
			$em->remove($entity);
			$em->flush();

			return $this->redirectToRoute($this->listRoute);
		}

		return $this->render('@TheNextCore/Admin/Company/show.html.twig', [
			'entity' => $entity,
			'delete_form' => $form->createView(),
			'list_route' => $this->listRoute,
			'edit_route' => $this->editRoute,
			'token' => $token
		]);
	}

	/**
	 * @ParamConverter("entity")
	 */
	public function editAction(Company $entity, Request $request)
	{
		return $this->formAction($this->createForm(AdminCompanyType::class, $entity), $request, function($form, Company $company, $thing) {
            $company->getSubscription()->setCompany($company);
        });
	}

	public function createAction(Request $request)
	{
	    $companyConfig = $this->get('company_config');
		return $this->formAction($this->createForm(AdminCompanyType::class, new Company()), $request, function($form, Company $company, $thing) use($companyConfig) {
		    $company->getSubscription()->setCompany($company);
            $companyConfig->applySiteSpecificOptions($company);
        });
	}
}