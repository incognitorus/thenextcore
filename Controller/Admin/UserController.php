<?php
/**
 * User: Jos Craaijo
 * Date: 11-7-2016
 */

namespace TheNextSoftware\CoreBundle\Controller\Admin;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use TheNextSoftware\CoreBundle\Entity\User;
use TheNextSoftware\CoreBundle\Form\Type\Admin\AdminUserType;
use TheNextSoftware\CoreBundle\Form\Type\Admin\AdminUserPasswordType;

class UserController extends BaseAdminController
{
	public function __construct()
	{
		$this->showRoute = 'admin_user_show';
		$this->listRoute = 'admin_user_list';
		$this->editRoute = 'admin_user_edit';
		$this->createRoute = 'admin_user_create';
	}

	/**
	 * @ParamConverter("user")
	 */
	public function companiesAction(User $user)
	{
		return $this->render('TheNextCoreBundle:Admin/Company:index.html.twig', [
			'entities' => $this->getDoctrine()->getRepository('TheNextCoreBundle:Company')->findBy([ 'owner' => $user ]),
			'create_route' => $this->createRoute,
			'show_route' => $this->showRoute,
			'show_add' => false
		]);
	}

	public function indexAction()
	{
		return $this->render('TheNextCoreBundle:Admin/User:index.html.twig', [
			'entities' => $this->getAllFromRepo('TheNextCoreBundle:User'),
			'create_route' => $this->createRoute,
			'show_route' => $this->showRoute
		]);
	}

	/**
	 * @ParamConverter("entity")
	 */
	public function showAction(User $entity, Request $request)
	{
		return $this->showHelper($this->createDeleteForm($entity), $entity, '@TheNextCore/Admin/User/show.html.twig', $request);
	}

	/**
	 * @ParamConverter("entity")
	 */
	public function editAction(User $entity, Request $request)
	{
		return $this->formAction($this->createForm(AdminUserType::class, $entity), $request);
	}

	/**
	 * @ParamConverter("entity")
	 */
	public function editPasswordAction(User $entity, Request $request)
	{
		$self = $this;
		$data = $request->request->all();

		return $this->formAction($this->createForm(AdminUserPasswordType::class, $entity), $request, function ($form, User $user, $em) use($self, $data) {

			if ($data &&
				$data['user'] &&
				$data['user']['plainPassword'] &&
				$data['user']['plainPassword']['first']) {

				$user->setPlainPassword($data['user']['plainPassword']['first']);
				$self->get('newuser')->updatePassword($user);

				$em->persist($user);
				$em->flush();
			}
		});
	}

	public function createAction(Request $request)
	{
	    $me = $this;
		return $this->formAction($this->createForm(AdminUserType::class, new User()), $request, function ($form, User $user, $em) use($me) {
            $me->get('session')->getFlashBag()->add('notice', 'Het account is aangemaakt');

            $me->get('newuser')->addNewAccount($user);
        });
	}
}