<?php
/**
 * User: Jos Craaijo
 * Date: 11-7-2016
 */

namespace TheNextSoftware\CoreBundle\Controller\Admin;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use TheNextSoftware\CoreBundle\Entity\Promocode;
use TheNextSoftware\CoreBundle\Form\Type\Admin\AdminPromocodeType;

class PromocodeController extends BaseAdminController
{
	public function __construct()
	{
		$this->showRoute = 'admin_promocode_show';
		$this->listRoute = 'admin_promocode_list';
		$this->editRoute = 'admin_promocode_edit';
		$this->createRoute = 'admin_promocode_create';
	}

	public function indexAction()
	{
		return $this->render('TheNextCoreBundle:Admin/Promocode:index.html.twig', [
			'entities' => $this->getAllFromRepo('TheNextCoreBundle:Promocode'),
			'create_route' => $this->createRoute,
			'show_route' => $this->showRoute
		]);
	}

	/**
	 * @ParamConverter("entity")
	 */
	public function showAction(Promocode $entity, Request $request)
	{
		return $this->showHelper($this->createDeleteForm($entity), $entity, '@TheNextCore/Admin/Promocode/show.html.twig', $request);
	}

	/**
	 * @ParamConverter("entity")
	 */
	public function editAction(Promocode $entity, Request $request)
	{
		return $this->formAction($this->createForm(AdminPromocodeType::class, $entity), $request);
	}

	public function createAction(Request $request)
	{
		return $this->formAction($this->createForm(AdminPromocodeType::class, new Promocode()), $request);
	}
}