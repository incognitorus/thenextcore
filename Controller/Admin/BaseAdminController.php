<?php
/**
 * User: Jos Craaijo
 * Date: 11-7-2016
 */

namespace TheNextSoftware\CoreBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class BaseAdminController extends Controller
{
	protected $showRoute, $listRoute, $editRoute, $createRoute;

	protected function getAllFromRepo($repo)
	{
		return $this->getDoctrine()->getRepository($repo)->findAll();
	}

	protected function createDeleteForm($entity)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl($this->showRoute, [ 'id' => $entity->getId() ]))
			->setMethod('DELETE')
			->add('submit', SubmitType::class, array('label' => 'Verwijderen', 'attr' => [ 'class' => 'btn btn-danger']))
			->getForm();
	}

	protected function showHelper(FormInterface $form, $entity, $template, Request $request)
	{
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid() && $request->getMethod() == 'DELETE') {
			$em = $this->getDoctrine()->getManager();
			$em->remove($entity);
			$em->flush();

			return $this->redirectToRoute($this->listRoute);
		}

		return $this->render($template, [
			'entity' => $entity,
			'delete_form' => $form->createView(),
			'list_route' => $this->listRoute,
			'edit_route' => $this->editRoute
		]);
	}

	/**
	 * @param FormInterface $form
	 * @param Request $request
	 * @param Callable $process
	 * @param string $template
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	protected function formAction(FormInterface $form, Request $request, $process = null, $template = 'TheNextCoreBundle:Helpers:formEditor.html.twig')
	{
		$entity = $form->getData();
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();

			if($process)
			{
				$process($form, $entity, $em);
			}

			$em->persist($entity);
			$em->flush();

			return $this->redirect($this->generateUrl($this->showRoute, [ 'id' => $entity->getId() ]));
		}

		return $this->render($template, [
			"form" => $form->createView()
		]);
	}
}