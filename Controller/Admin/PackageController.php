<?php
/**
 * User: Jos Craaijo
 * Date: 11-7-2016
 */

namespace TheNextSoftware\CoreBundle\Controller\Admin;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use TheNextSoftware\CoreBundle\Entity\Package;

use TheNextSoftware\CoreBundle\Form\Type\Admin\AdminPackageType;

class PackageController extends BaseAdminController
{
	private $processingFunction;

	public function __construct()
	{
		$this->showRoute = 'admin_package_show';
		$this->listRoute = 'admin_package_list';
		$this->editRoute = 'admin_package_edit';
		$this->createRoute = 'admin_package_create';
		$this->processingFunction = function ($form, $entity, $em) {
			/** @var $entity Package */
			/** @var $form FormInterface */
			$entity->getFeatures()->clear();
			foreach($form->get('features')->getData() as $feature)
			{
				$entity->addFeature($feature);
			}
		};
	}

	public function indexAction()
	{
		return $this->render('TheNextCoreBundle:Admin/Package:index.html.twig', [
			'entities' => $this->getAllFromRepo('TheNextCoreBundle:Package'),
			'create_route' => $this->createRoute,
			'show_route' => $this->showRoute
		]);
	}

	/**
	 * @ParamConverter("entity")
	 */
	public function showAction(Package $entity, Request $request)
	{
		return $this->showHelper($this->createDeleteForm($entity), $entity, '@TheNextCore/Admin/Package/show.html.twig', $request);
	}

	/**
	 * @ParamConverter("entity")
	 */
	public function editAction(Package $entity, Request $request)
	{
		return $this->formAction($this->createForm(AdminPackageType::class, $entity), $request, $this->processingFunction);
	}

	public function createAction(Request $request)
	{
		return $this->formAction($this->createForm(AdminPackageType::class, new Package()), $request, $this->processingFunction);
	}
}