<?php

namespace TheNextSoftware\CoreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use SparkPost\APIResponseException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AuthController extends Controller {
  public function loginAction(Request $request) {
    $authenticationUtils = $this->get('security.authentication_utils');
    return $this->render(
      '@TheNextCore/Auth/login.html.twig',
      array(
        'last_username' => $authenticationUtils->getLastUsername(),
        'error'         => $authenticationUtils->getLastAuthenticationError(),
      )
    );
  }
}
