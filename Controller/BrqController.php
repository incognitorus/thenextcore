<?php
/**
 * User: Jos Craaijo
 * Date: 2-5-2016
 */

namespace TheNextSoftware\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TheNextSoftware\CoreBundle\Entity\Transaction;
use TheNextSoftware\CoreBundle\Entity\TransactionData;
use TheNextSoftware\CoreBundle\Type\EnumTransactionStatus;

class BrqController extends Controller
{
	public function handleBrqSuccessAction()
	{
		file_put_contents($this->get('kernel')->getRootDir() . '/files/success.' . time() . '.txt', json_encode(array(
			'POST'    => $_POST,
			'REQUEST' => $_REQUEST,
			'GET'     => $_GET
		)));

		return new Response('success');
	}

	public function handleBrqPushAction(Request $request)
	{
		$logger = $this->get('logger');
		$logger->addAlert('Received BRQ push');

		$em = $this->getDoctrine()->getManager();

		$data = array();
		foreach ($request->request->all() as $k => $v)
			if (substr($k, 0, 4) == 'brq_')
				$data[substr($k, 4)] = $v;

		// test code
		$filename = $this->get('kernel')->getRootDir() . '/files/push.' . time() . '.txt';
		echo $filename . ";;";
		file_put_contents($filename, json_encode($data), FILE_APPEND);
		// /test code

		if (empty($data['invoicenumber']))
			return new Response((string)file_put_contents($filename, 'empty invoice', FILE_APPEND));

		/** @var Transaction $transaction */
		$transaction = $em->getRepository("TheNextCoreBundle:Transaction")->findOneBy([ 'transactionId' => $data['invoicenumber'] ]);
		if (!$transaction)
			return new Response((string)file_put_contents($filename, 'no transaction', FILE_APPEND));

		if ($data['statuscode'] == 190)
		{
			$logger->addAlert('Status = 190');
			$transaction->setStatus(EnumTransactionStatus::STATUS_SUCCESS);
		} else if (in_array($data['statuscode'], array(490, 491, 492, 690)))
		{
			$transaction->setStatus(EnumTransactionStatus::STATUS_FAILED);
		} else if (in_array($data['statuscode'], array(890, 891)))
		{
			$transaction->setStatus(EnumTransactionStatus::STATUS_FAILED);
		} else if (in_array($data['statuscode'], array(791)))
		{
			$transaction->setStatus(EnumTransactionStatus::STATUS_PENDING);
		}else{
			return new Response('Invalid transaction code');
		}

		$transactionData = $this->getDoctrine()->getRepository("TheNextCoreBundle:TransactionData")
            ->findOneBy([ 'transaction' => $transaction ]);

		if(!$transactionData)
        {
            $transactionData = new TransactionData();
        }

		$transactionData->setData(json_encode($data));

		$transactionData->setTransaction($transaction);
		$transaction->setData($transactionData);

		$subscription = $transaction->getSubscription();

		$logger->addAlert('Type: ' . $transaction->getType() . ', status: ' . $transaction->getStatus() . ' -- ' . ($transaction->getStatus() == EnumTransactionStatus::STATUS_SUCCESS) . ' -- ' . ($transaction->getType() == 'confirmation'));

		if ($transaction->getType() == 'confirmation')
		{
			if ($transaction->getStatus() == EnumTransactionStatus::STATUS_SUCCESS)
			{
				$subscription->setBrqInvoiceNumber($data['invoicenumber']);
				$subscription->setBrqConsumerBic($data['service_ideal_consumerbic']);
				$subscription->setBrqConsumerIban($data['service_ideal_consumeriban']);
				$subscription->setBrqConsumerIssuer($data['service_ideal_consumerissuer']);
				$subscription->setBrqConsumerName($data['service_ideal_consumername']);
				$subscription->setBrqTimestamp($data['timestamp']);
				$subscription->setBrqTransactions($data['transactions']);

				$logger->addAlert('Added new subscription: ' . print_r($data, TRUE));
			}
		}

		$em->persist($transactionData);
		$em->persist($transaction);
		$em->persist($subscription);
		$em->flush();

		$logger->addAlert('Processed BRQ push');
		return new Response("Success");
	}
}