<?php
/**
 * User: Jos
 * Date: 21-11-2015
 */

namespace TheNextSoftware\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use TheNextSoftware\CoreBundle\Entity\Company;

abstract class BaseCompanyController extends Controller
{
	/**
	 * @var Company
	 */
	private $company = NULL;

	public function setCompany($company_id)
	{
		if($company_id != NULL)
		{
			$this->company = $this->getDoctrine()
				->getRepository('TheNextCoreBundle:Company')
				->find($company_id);

			$this->denyAccessUnlessGranted('view', $this->company);
		}
	}

	public function getCompany()
	{
		if($this->company == NULL)
		{
			throw $this->createAccessDeniedException();
		}else{
			return $this->company;
		}
	}
}
