<?php
/**
 * User: Jos Craaijo
 * Date: 11-7-2016
 */

namespace TheNextSoftware\CoreBundle\Controller;

use Doctrine\Common\Collections\Criteria;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use TheNextSoftware\CoreBundle\Entity\Subscription;
use TheNextSoftware\CoreBundle\Entity\Transaction;
use TheNextSoftware\CoreBundle\Entity\User;
use TheNextSoftware\CoreBundle\Entity\UserRole;
use TheNextSoftware\CoreBundle\Form\Type\PackageCompanyType;

use TheNextSoftware\CoreBundle\Form\Type\UserRoleType;
use TheNextSoftware\CoreBundle\Repository\PackageRepository;
use TheNextSoftware\CoreBundle\Type\EnumSubscriptionStatus;
use TheNextSoftware\CoreBundle\Type\EnumTransactionStatus;

class CompanyController extends BaseCompanyController
{
	public function homeAction()
	{
		return $this->render(
			'@TheNextCore/Company/home.html.twig',
			[
				'company' => $this->getCompany()
			]
		);
	}
	public function createSuccessAction()
	{
		$company = $this->getCompany();

		return $this->render("@TheNextCore/Company/creationSuccess.html.twig", [
			"company" => $company
		]);
	}

	// Select package
	public function activateAction(Request $request)
	{
		$this->denyAccessUnlessGranted('edit', $this->getCompany());

		$em = $this->getDoctrine()->getManager();
		$company = $this->getCompany();

		if($company->getPackage() != null)
		{
			return $this->redirectToRoute("company_home", [
				"company_id" => $company->getId()
			]);
		}

		$packageRepo = $em->getRepository("TheNextCoreBundle:Package");
		$package = $packageRepo->findOneBy([ "isVisible" => true ]);

		if(!$package)
		{
			// Er zijn geen packages geregistreerd, redirect naar restaurant home.
			return $this->redirectToRoute('company_home', ['company_id' => $company->getId()]);
		}

		$form = $this->createForm(PackageCompanyType::class, $company);

		$form->handleRequest($request);
		if($form->isValid() && $form->isSubmitted())
		{
			$company->setPackageSetOn(new \DateTime());
			$em = $this->getDoctrine()->getManager();
			$em->persist($company);
			$em->flush();

			return $this->redirectToRoute('company_home', [ 'company_id' => $company->getId() ]);
		}

		$em = $this->getDoctrine();
		/** @var PackageRepository $repo */
		$repo = $em->getRepository("TheNextCoreBundle:Package");

		return $this->render(
			'@TheNextCore/Company/activate.html.twig',
			[
				'packages' => $repo->getVisiblePackages(),
				'form' => $form->createView()
			]
		);
	}

	public function finishSubscriptionAction($transaction_id)
	{
		$company = $this->getCompany();
		$subscription = $company->getSubscription();

		if($subscription == null)
		{
			return $this->redirectToRoute("company_start_subscription", [
				"company_id" => $company->getId()
			]);
		}

		if(!$subscription->isValid())
		{
			/** @var Transaction $transaction */
			$transaction = $subscription->getTransactions()->matching(Criteria::create()->where(Criteria::expr()->eq('transactionId', $transaction_id)))->first();

			$em = $this->getDoctrine()->getManager();

			if ($transaction)
			{
				if ($transaction->getStatus() == EnumTransactionStatus::STATUS_SUCCESS)
				{
					// TODO: Twinfield nieuweDebiteur
					$debcode = "123ABC";

					$subscription->setStatus(EnumSubscriptionStatus::STATUS_ACTIVE);
					$subscription->setDebcode($debcode);

					$em->persist($subscription);
					$em->flush();
				} else
				{
					$subscription->setStatus(EnumSubscriptionStatus::STATUS_CANCELLED);
					$subscription->setCancelledPer(new \DateTime());
				}

				$em->persist($subscription);
				$em->flush();
			} else
			{
				throw new AccessDeniedException("Invalid transactionID");
			}
		}

		return $this->render(
			'@TheNextCore/Company/subscription_finish.html.twig',
			[
				'status' => $subscription->getStatus(),
				'company' => $this->getCompany()
			]
		);
	}

	// iDeal transaction
	public function startSubscriptionAction(Request $request)
	{
		$this->denyAccessUnlessGranted('edit', $this->getCompany());

		$company = $this->getCompany();
		if($company->getSubscription() != null && ($company->getSubscription()->isValid()))
		{
			return $this->redirectToRoute("company_home", [
				'company_id' => $company->getId()
			]);
		}

		$form = $this->createFormBuilder();
		$form->add('submit', SubmitType::class, [ 'label' => 'Doorgaan' ]);

		$form = $form->getForm();

		$form->handleRequest($request);
		if($form->isValid() && $form->isSubmitted())
		{
			$buckaroo = $this->get('app.services.buckaroo_loader');

			$subscription = new Subscription();

			$transactionId = uniqid('tns-');
			$em = $this->getDoctrine()->getManager();

			$company = $this->getCompany();
			$oldSubs = $em->getRepository("TheNextCoreBundle:Subscription")->findBy([ 'company' => $company ]);
			foreach($oldSubs as $sub)
			{
				$em->remove($sub);
			}
			$em->flush();

			$company->setSubscription($subscription);
			$subscription->setCompany($company);
			$subscription->setStatus(EnumSubscriptionStatus::STATUS_PENDING);

			$transaction = new Transaction();
			$transaction->setSubscription($subscription);
			$transaction->setType("confirmation");
			$transaction->setTransactionId($transactionId);
			$transaction->setStatus(EnumTransactionStatus::STATUS_PENDING);
			$subscription->addTransaction($transaction);

			$em->persist($subscription);
			$em->flush();

			$companyId = $this->getCompany()->getId();

			$url = $this->generateUrl("company_finish_subscription", [
				'company_id' => $companyId,
				'transaction_id' => $transactionId
			], UrlGeneratorInterface::ABSOLUTE_URL);

			return $this->render('@TheNextCore/buckaroo_passthrough.html.twig', [
				'data' => $buckaroo->createRequestData([
					'brq_currency'       => 'EUR',
					'brq_amount'         => '0.01',
					'brq_invoicenumber'  => $transactionId,
					'brq_description'    => 'TheNextSoftware Test transactie',
					'brq_culture'        => 'NL',
					'brq_return'         => $url,
					'brq_returncancel'   => $url,
					'brq_returnerror'    => $url,
					'brq_returnreject'   => $url
				]),
				'url' => $buckaroo->getUrl()
			]);
		}

		return $this->render(
			'@TheNextCore/Company/subscription_start.html.twig',
			[
				'form' => $form->createView()
			]
		);
	}

	public function upgradeAction(Request $request)
	{
		$this->denyAccessUnlessGranted('edit', $this->getCompany());

		$company = $this->getCompany();
		$currentPackage = $company->getPackage();

		$form = $this->createForm(PackageCompanyType::class, $company);

		$form->handleRequest($request);
		if($form->isValid() && $form->isSubmitted())
		{
			if($form->get('stop')->isClicked())
			{
				$company->getSubscription()->setStatus('cancelled');

				$this->get('session')->getFlashBag()->set('notice', $this->get('translator')->trans('company.subscription.cancelled'));

				$em = $this->getDoctrine()->getManager();
				$em->persist($company->getSubscription());
				$em->flush();
			}else
			{
				if ($currentPackage == NULL
					|| $company->getPackage()->getPrice() >= $currentPackage->getPrice()
					|| $company->getPackageSetOn()->diff(new \DateTime())->days >= 31
				)
				{
					$company->setPackageSetOn(new \DateTime());

					$em = $this->getDoctrine()->getManager();
					$em->persist($company);
					$em->flush();

					return $this->redirectToRoute('company_home', ['company_id' => $company->getId()]);
				} else
				{
					$form->get("package")->addError(
						new FormError("Om misbruik te voorkomen, kan het pakket maar één keer per maand naar een lager pakket worden aangepast. Upgraden naar een hoger pakket kan altijd."));
					$company->setPackage($currentPackage);
				}
			}
		}

		$em = $this->getDoctrine();
		/** @var PackageRepository $repo */
		$repo = $em->getRepository("TheNextCoreBundle:Package");

		return $this->render(
			'@TheNextCore/Company/upgradePackageMessage.twig',
			[
				'packages' => $repo->getVisiblePackages(),
				'form' => $form->createView(),
				'company' => $company
			]
		);
	}

	public function editEmployeesAction(Request $request)
	{
		$this->denyAccessUnlessGranted('edit', $this->getCompany());

		$form = $this->createFormBuilder([])
			->add('email', EmailType::class, [
				'label' => 'E-mail adres',
                'required' => true,
                'constraints' => [
                    new Email(),
                    new NotBlank(),
                ]
			])
			->add('first_name', TextType::class, [
				'label' => 'Voornaam',
                'empty_data' => '',
			])
			->add('last_name', TextType::class, [
				'label' => 'Achternaam',
                'empty_data' => '',
			])
			->add('submit', SubmitType::class, [
				'label' => 'Medewerker toevoegen'
			])
			->getForm();

		$form->handleRequest($request);
		if($form->isValid() && $form->isSubmitted())
		{
			$em = $this->getDoctrine()->getManager();
			$em->flush();

			$data = $form->getData();

			$em = $this->getDoctrine()->getManager();

			$userRepo = $em->getRepository('TheNextCoreBundle:User');
			$user = $userRepo->findOneBy([ 'email' => $data['email'] ]);

			$newAccount = $user == null;
			if($newAccount)
			{
				// Create new user
				$user = new User();
				$user->setFirstName($data['first_name']);
				$user->setLastName($data['last_name']);
				$user->setEmail($data['email']);
				$this->get('newuser')->addNewAccount($user);
			}

			if($this->getCompany()->getEmployees()->filter(function ($item) use($user) {
					/** @var $item UserRole */
					return $item->getUser() == $user;
				})->count() > 0)
			{
				$form->get('email')->addError(new FormError("Deze mederwerker is al aan het restaurant toegevoegd."));
			}else if($user == $this->getCompany()->getOwner())
			{
				$form->get('email')->addError(new FormError("Je kan jezelf niet als medewerker toevoegen."));
			}else
			{
				$role = new UserRole();
				$role->setCompany($this->getCompany());
				$role->setUser($user);

				$this->getCompany()->addEmployee($role);

				$em->persist($role);
				$em->flush();

				if(!$newAccount)
				{
					$this->get('newuser')->mailAddedToRestaurant($user, $this->getCompany());
				}

				return $this->redirect($this->generateUrl("company_employee_list", ['company_id' => $this->getCompany()->getId()]));
			}
		}

		return $this->render(
			'@TheNextCore/Company/Employee/employees.html.twig',
			[
				'form' => $form->createView(),
				'employees' => $this->getCompany()->getEmployees()
			]
		);
	}

	/**
	 * @ParamConverter("role", options={"id" = "role_id"})
	 */
	public function editEmployeeAction(UserRole $role, Request $request)
	{
		$this->denyAccessUnlessGranted('edit', $this->getCompany());

		$form = $this->createForm(UserRoleType::class, $role);

		$form->handleRequest($request);
		if($form->isValid() && $form->isSubmitted())
		{
			$em = $this->getDoctrine()->getManager();
			if($form->get('save')->isClicked())
			{
				$em->persist($role);
			}else if($form->get('remove')->isClicked())
			{
				$em->remove($role);
			}

			$em->flush();

			return $this->redirect($this->generateUrl('company_employee_list', ['company_id' => $this->getCompany()->getId()]));
		}

		return $this->render(
			'@TheNextCore/Company/Employee/employeeEdit.html.twig',
			[
				'form' => $form->createView(),
				'role' => $role
			]
		);
	}
}