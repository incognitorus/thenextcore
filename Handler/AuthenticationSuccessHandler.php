<?php
/**
 * User: Jos
 * Date: 16-11-2015
 */
namespace TheNextSoftware\CoreBundle\Handler;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;
use TheNextSoftware\CoreBundle\Entity\User;

class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler {

	/** @var EntityManager  */
	protected $em;

	public function __construct( EntityManager $entityManager, HttpUtils $httpUtils, array $options ) {
		$this->em = $entityManager;
		parent::__construct( $httpUtils, $options );
	}

	public function onAuthenticationSuccess( Request $request, TokenInterface $token )
	{
		/** @var User $user */
		$user = $this->em->getRepository('TheNextCoreBundle:User')->findOneById($token->getUser()->getId());
		$user->setLastLogin(new \DateTime());
		$this->em->flush();

		return parent::onAuthenticationSuccess( $request, $token );
	}
}