<?php
/**
 * User: Jos Craaijo
 * Date: 12-7-2016
 */

namespace TheNextSoftware\CoreBundle\Security\Authorization\Voter;

use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use TheNextSoftware\CoreBundle\Entity\Company;
use TheNextSoftware\CoreBundle\Entity\User;
use TheNextSoftware\CoreBundle\Entity\UserRole;

class CompanyVoter extends Voter
{
	const VIEW = 'view';
	const EDIT = 'edit';
	const OWNER = 'owner';

	protected function getSupportedAttributes()
	{
		return array(self::VIEW, self::EDIT, self::OWNER);
	}

	/**
	 * Determines if the attribute and subject are supported by this voter.
	 *
	 * @param string $attribute An attribute
	 * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
	 *
	 * @return bool True if the attribute and subject are supported, false otherwise
	 */
	protected function supports($attribute, $subject)
	{
		return $subject instanceof Company && $this->supportsAttribute($attribute);
	}

	/**
	 * Perform a single access check operation on a given attribute, subject and token.
	 *
	 * @param string $attribute
	 * @param Company $company
	 * @param TokenInterface $token
	 * @return bool
	 */
	public function voteOnAttribute($attribute, $company, TokenInterface $token)
	{
		$user = $token->getUser();

		if (!$user instanceof User) {
		    return false;
		}

		if ($user->getIsAdmin() || $user->getId() === $company->getOwnerId()) {
			return true;
		}

		if($attribute != self::OWNER)
		{
			$employees = $company->getEmployees()
				->matching(Criteria::create()
					->where(Criteria::expr()->eq('user', $user)));

			/** @var UserRole $employee */
			$employee = $employees->first();

			switch ($attribute)
			{
				case self::VIEW:
					return $employee != null;
				case self::EDIT:
					return $employee->hasManageAccess();
			}
		}

		return false;
	}

	public function supportsAttribute($attribute)
	{
		return in_array($attribute, $this->getSupportedAttributes());
	}

	public function supportsClass($class)
	{
		return $class == Company::class;
	}
}