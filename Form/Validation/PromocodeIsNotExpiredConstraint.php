<?php
/**
 * Created by PhpStorm.
 * User: Jos Craaijo
 * Date: 12-1-2017
 * Time: 15:59
 */

namespace TheNextSoftware\CoreBundle\Form\Validation;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;

class PromocodeIsNotExpiredConstraint extends Constraint
{
    public $message = 'Deze promocode kan niet meer gebruikt worden';
}