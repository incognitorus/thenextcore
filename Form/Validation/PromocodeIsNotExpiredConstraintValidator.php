<?php
/**
 * Created by PhpStorm.
 * User: Jos Craaijo
 * Date: 12-1-2017
 * Time: 16:02
 */

namespace TheNextSoftware\CoreBundle\Form\Validation;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PromocodeIsNotExpiredConstraintValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if($value == null)
        {
            return;
        }

        if($value->getExpiresOn() < new \DateTime())
        {
            $this->context->buildViolation($constraint->message)
                ->atPath('')
                ->addViolation();
        }

        $promocodeActivationRepo = $this->entityManager->getRepository('TheNextCoreBundle:PromocodeActivation');

        if($value->getMaxUses() < count($promocodeActivationRepo->findBy([
                'promocode' => $value
            ]))
        )
        {
            $this->context->buildViolation($constraint->message)
                ->atPath('')
                ->addViolation();
        }
    }
}