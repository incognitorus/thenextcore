<?php
/**
 * User: Jos
 * Date: 16-11-2015
 */
namespace TheNextSoftware\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name', TextType::class, ['label' => 'Naam'])
			->add('address', TextType::class, ['label' => 'Adres'])
			->add('city', TextType::class, ['label' => 'Stad'])
			->add('zipcode', TextType::class, ['label' => 'Postcode'])
            ->add('phoneNumber', TextType::class, ['label' => 'Telefoon'])
			->add('kvk_number', TextType::class, ['label' => 'KVK nummer'])
			->add('iban_number', TextType::class, ['label' => 'IBAN nummer'])
			->add('save', SubmitType::class, ['label' => 'Opslaan']);
	}

	public function getBlockPrefix()
	{
		return 'company';
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'TheNextSoftware\CoreBundle\Entity\Company',
		));
	}
}