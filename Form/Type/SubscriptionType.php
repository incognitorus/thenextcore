<?php
/**
 * User: Jos
 * Date: 16-11-2015
 */
namespace TheNextSoftware\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TheNextSoftware\CoreBundle\Type\EnumSubscriptionStatus;

class SubscriptionType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('status', ChoiceType::class, [
			    'label' => 'Status',
                'choices' => [
                    EnumSubscriptionStatus::STATUS_ACTIVE => EnumSubscriptionStatus::STATUS_ACTIVE,
                    EnumSubscriptionStatus::STATUS_CANCELLED => EnumSubscriptionStatus::STATUS_CANCELLED,
                    EnumSubscriptionStatus::STATUS_DEMO => EnumSubscriptionStatus::STATUS_DEMO,
                    EnumSubscriptionStatus::STATUS_PENDING => EnumSubscriptionStatus::STATUS_PENDING,
                ]
            ])
			->add('save', SubmitType::class, ['label' => 'Opslaan']);
	}

	public function getBlockPrefix()
	{
		return 'subscription';
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'TheNextSoftware\CoreBundle\Entity\Subscription',
		));
	}
}