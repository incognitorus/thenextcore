<?php
/**
 * User: Jos Craaijo
 * Date: 14-1-2016
 */
namespace TheNextSoftware\CoreBundle\Form\Type;

use Doctrine\Common\Collections\Criteria;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TheNextSoftware\CoreBundle\Repository\PackageRepository;

class PackageCompanyType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('package', EntityType::class, [
				'class' => 'TheNextSoftware\CoreBundle\Entity\Package',
				'label' => 'Pakket',
				'query_builder' => function ($repo) {
					/** @var PackageRepository $repo */
					$qb = $repo->createQueryBuilder('u');

					$visiblePackages = new Criteria();
					$visiblePackages->where($visiblePackages->expr()->eq("isVisible", TRUE));

					$qb->addCriteria($visiblePackages);
					return $qb;
				}
			])
			->add('save', SubmitType::class, ['label' => 'Opslaan'])
			->add('stop', SubmitType::class, [
				'label' => 'Abonnement opzeggen',
				'attr' => [
					'class' => 'btn-danger'
				]
			])
		;
	}

	public function getBlockPrefix()
	{
		return 'company_package';
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'TheNextSoftware\CoreBundle\Entity\Company',
		));
	}
}