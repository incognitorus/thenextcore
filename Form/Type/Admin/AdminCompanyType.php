<?php

namespace TheNextSoftware\CoreBundle\Form\Type\Admin;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TheNextSoftware\CoreBundle\Entity\Package;
use TheNextSoftware\CoreBundle\Entity\User;
use TheNextSoftware\CoreBundle\Form\Type\SubscriptionType;

class AdminCompanyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Restaurantnaam'])
            ->add('address', TextType::class, ['label' => 'Adres'])
            ->add('city', TextType::class, ['label' => 'Stad'])
            ->add('zipcode', TextType::class, ['label' => 'Postcode'])
            ->add('kvk_number', TextType::class, ['label' => 'KVK nummer'])
            ->add('iban_number', TextType::class, ['label' => 'IBAN nummer', 'required' => false])
            ->add('phoneNumber', TextType::class, ['label' => 'Telefoonnummer'])
            ->add('owner', EntityType::class, ['label' => 'Eigenaar', 'class' => User::class])
            ->add('package', EntityType::class, ['label' => 'Pakket', 'class' => Package::class])
            ->add('subscription', SubscriptionType::class, ['label' => 'Abonnement'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TheNextSoftware\CoreBundle\Entity\Company'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'core_company';
    }
}
