<?php

namespace TheNextSoftware\CoreBundle\Form\Type\Admin;


use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use TheNextSoftware\CoreBundle\Service\PackageFeatureManager;

class AdminPackageType extends AbstractType
{
    private $featureManager;

    public function __construct(PackageFeatureManager $featureManager)
    {
        $this->featureManager = $featureManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $features = $this->featureManager->getFeatures();

        $builder
            ->add('name', null, [ 'label' => 'Naam' ])
            ->add('features', ChoiceType::class, [
                'label' => 'Features',
                'multiple' => true,
                'mapped' => false,
                'required' => false,
                'choices' => array_combine($features, $features)
            ])
            ->add('isVisible', null, [ 'label' => 'Zichtbaar voor nieuwe klanten' ])
            ->add('price', null,  [ 'label' => 'Prijs (cent)' ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TheNextSoftware\CoreBundle\Entity\Package'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'core_package';
    }
}
