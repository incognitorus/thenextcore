<?php
/**
 * User: Jos Craaijo
 * Date: 5-7-2016
 */

namespace TheNextSoftware\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRoleType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('manage_access', CheckboxType::class, [
				'label' => 'Beheerrechten',
				'required' => false,
			])
			->add('save', SubmitType::class, [
				'label' => 'Opslaan'
			])
			->add('remove', SubmitType::class, [
				'label' => 'Toegang ontzeggen',
				'attr' => [
					'class' => 'btn-danger'
				]
			]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'TheNextSoftware\CoreBundle\Entity\UserRole'
		));
	}

	public function getBlockPrefix()
	{
		return 'user_role';
	}
}