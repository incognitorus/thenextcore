<?php
/**
 * Created by PhpStorm.
 * User: Oleg Lukashev
 * Date: 24-03-2017
 * Time: 14:44
 */

namespace AppBundle\Form\ApiV2;


use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ApiUserType extends ApiBaseType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'E-mail'
            ])
            ->add('first_name', TextType::class, [
                'label' => 'Voornaam'
            ])
            ->add('last_name', TextType::class, [
                'label' => 'Achternaam'
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options'  => ['label' => 'Wachtwoord'],
                'second_options' => ['label' => 'Wachtwoord herhalen'],
                'constraints' => [
                    new NotBlank(),
                    new Length([ 'min' => 8, 'max' => 4096 ])
                ]
                ]
            )
            ->add('save', SubmitType::class, [
                'label' => 'Account aanmaken'
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TheNextSoftware\CoreBundle\Entity\User',
            'csrf_protection' => false
        ));
    }

    public function getBlockPrefix()
    {
        return 'user';
    }
}
