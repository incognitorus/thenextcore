<?php
/**
 * User: Jos
 * Date: 23-11-2015
 */

namespace AppBundle\Form\ApiV2;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ApiUserUpdatePasswordType extends ApiBaseType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('plainPassword', RepeatedType::class, [
					'type' => PasswordType::class,
					'first_options'  => ['label' => 'Wachtwoord'],
					'second_options' => ['label' => 'Wachtwoord herhalen'],
					'constraints' => [
						new NotBlank(),
						new Length([ 'min' => 8, 'max' => 4096 ])
					]
				]
			)
			->add('save', SubmitType::class, [
                'label' => 'Account aanmaken'
            ]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'TheNextSoftware\CoreBundle\Entity\User',
            'csrf_protection' => false
		));
	}

	public function getBlockPrefix()
	{
		return 'user';
	}
}
