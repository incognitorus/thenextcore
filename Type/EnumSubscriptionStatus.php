<?php
/**
 * User: Jos Craaijo
 * Date: 27-4-2016
 */

namespace TheNextSoftware\CoreBundle\Type;


class EnumSubscriptionStatus extends EnumType
{
	const STATUS_ACTIVE = 'active';
	const STATUS_CANCELLED = 'cancelled';
	const STATUS_DEMO = 'demo';
	const STATUS_PENDING = 'pending';

	protected $name = 'enum_subscription_status';
	protected $values = [
		self::STATUS_ACTIVE,
		self::STATUS_CANCELLED,
		self::STATUS_DEMO,
		self::STATUS_PENDING,
	];
}