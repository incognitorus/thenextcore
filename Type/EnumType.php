<?php
/**
 * User: Jos
 * Date: 23-11-2015
 */

namespace TheNextSoftware\CoreBundle\Type;

use Doctrine\DBAL\Platforms\SqlitePlatform;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

abstract class EnumType extends Type
{
	protected $name;
	protected $values = array();

	public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
	{
		if($platform instanceof SqlitePlatform)
		{
			return "VARCHAR(" . max(array_map(function ($item) {
				return strlen($item);
			}, $this->values)) . ")";
		}else
		{
			$values = array_map(function ($val)
			{
				return "'" . $val . "'";
			}, $this->values);

			return "ENUM(" . implode(", ", $values) . ") COMMENT '(DC2Type:" . $this->name . ")'";
		}
	}

	public function convertToPHPValue($value, AbstractPlatform $platform)
	{
		return $value;
	}

	public function convertToDatabaseValue($value, AbstractPlatform $platform)
	{
		if (!in_array($value, $this->values)) {
			throw new \InvalidArgumentException("Invalid '".$this->name."' value: " . $value . ". Valid values are: { " . join(", ", $this->values) . " }");
		}
		return $value;
	}

	public function getName()
	{
		return $this->name;
	}
}