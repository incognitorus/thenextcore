<?php
/**
 * User: Jos Craaijo
 * Date: 2-5-2016
 */

namespace TheNextSoftware\CoreBundle\Type;


class EnumTransactionStatus extends EnumType
{
	const STATUS_FAILED = 'failed';
	const STATUS_PENDING = 'pending';
	const STATUS_SUCCESS = 'success';

	protected $name = 'enum_transaction_status';
	protected $values = [
		self::STATUS_FAILED,
		self::STATUS_PENDING,
		self::STATUS_SUCCESS
	];
}