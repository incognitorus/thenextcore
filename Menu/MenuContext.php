<?php
/**
 * User: Jos Craaijo
 * Date: 11-7-2016
 */

namespace TheNextSoftware\CoreBundle\Menu;


use Knp\Menu\MenuItem;

class MenuContext
{
	/** @var  MenuItem */
	public $item;

	public $parent = null;

	public $parameters = [];

	public $route = null;

	public $icon = null;

	public $matchRoutes = [];

	public $name;

	private $visible = true;

	public function __construct($name, $route)
	{
		$this->route = $route;
		$this->matchRoutes = [ $route ];
		$this->name = $name;
	}

	public function withIcon($icon)
	{
		$this->icon = $icon;

		return $this;
	}

	public function hide()
	{
		$this->visible = false;

		return $this;
	}

	public function addRouteMatch($route)
	{
		$this->matchRoutes[] = $route;

		return $this;
	}

	public function addParameter($name, $value)
	{
		$this->parameters[$name] = $value;

		return $this;
	}

	public function build()
	{
		if($this->icon)
		{
			$this->item->setAttribute('icon', $this->icon);
		}

		$this->item->setDisplay($this->visible);

		if($this->matchRoutes)
		{
			$this->item->setExtra('match-routes', $this->matchRoutes);
		}
	}
}