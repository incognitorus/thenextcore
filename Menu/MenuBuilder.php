<?php
/**
 * User: Jos Craaijo
 * Date: 4-1-2016
 */

namespace TheNextSoftware\CoreBundle\Menu;


use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

use Symfony\Component\Translation\Translator;
use TheNextSoftware\CoreBundle\Entity\Company;
use TheNextSoftware\CoreBundle\Entity\User;
use TheNextSoftware\CoreBundle\Entity\UserRole;

class MenuBuilder
{
	protected $translator;
	private $factory, $em, $tokenStorage;

	public function __construct(FactoryInterface $factory, EntityManager $em, TokenStorage $tokenStorage, Translator $translator)
	{
		$this->factory = $factory;
		$this->em = $em;
		$this->tokenStorage = $tokenStorage;
		$this->translator = $translator;
	}

	public function createMainMenu(array $options)
	{
		/** @var User $user */
		$user = $this->tokenStorage->getToken()->getUser();

        if($user->getIsAdmin())
        {
            $company = $this->em->getRepository('TheNextCoreBundle:Company')->find((int)$options['company_id']);
        }else{
            /** @var Company $company */
            $company = $user->getCompanies()
                ->matching(Criteria::create()->where(Criteria::expr()->eq("id", (int)$options['company_id'])))
                ->first();
        }

		$this->begin(
			(new MenuContext($this->translator->trans('Start'), ''))
			->addParameter('company_id', $company ? $company->getId() : 0)
		);

		if($company !== false && $company !== null)
		{
			$this->add(
				(new MenuContext($this->translator->trans('Dashboard'), 'company_home'))
					->withIcon('fa-tachometer')
			);

			$this->fillCompanyMenu($company, $user);

            $employees = $company->getEmployees()
                ->matching(Criteria::create()
                    ->where(Criteria::expr()->eq('user', $user)));

            /** @var UserRole $employee */
            $employee = $employees->first();

            if($user == $company->getOwner() || ($employee !== false && $employee->hasManageAccess()) || $user->getIsAdmin())
            {
                $this->begin(
                    (new MenuContext($this->translator->trans('Bedrijf beheren'), ''))
                        ->withIcon('fa-wrench'));
                {
                    $this->fillCompanyManageMenu($company, $user);

                    $this->add(
                        (new MenuContext($this->translator->trans('Pakket upgraden'), 'company_upgrade'))
                            ->hide());

                    $this->add(
                        (new MenuContext($this->translator->trans('Pakket activeren'), 'company_activate'))
                            ->hide());

                    $this->add(
                        (new MenuContext($this->translator->trans('Pakket aanpassen'), 'company_upgrade'))
                            ->withIcon('fa-cogs'));

                    $this->add(
                        (new MenuContext($this->translator->trans('Medewerkers'), 'company_employee_list'))
                            ->withIcon('fa-user')
                            ->addRouteMatch('company_employee_edit'));
                }
                $this->end();
            }
		}

		return $this->end();
	}

	/**
	 * @param $company Company
	 * @param $user User
	 */
	protected function fillCompanyMenu($company, $user)
	{ }

	/**
	 * @param $company Company
	 * @param $user User
	 */
	protected function fillCompanyManageMenu($company, $user)
	{ }

	public function createAdminMenu(array $options)
	{
		$this->begin(new MenuContext('Start', ''));

		/** @var User $user */
		$user = $this->tokenStorage->getToken()->getUser();

		if($user->getIsAdmin())
		{
			$this->fillAdminMenu();
		}

		return $this->end();
	}

	protected function fillAdminMenu()
	{
		$this->add(
			(new MenuContext('Promocodes', 'admin_promocode_list'))
				->withIcon('fa-plus')
				->addRouteMatch('admin_promocode_edit')
				->addRouteMatch('admin_promocode_create')
				->addRouteMatch('admin_promocode_show')
		);

		$this->add(
			(new MenuContext('Accounts', 'admin_user_list'))
				->withIcon('fa-plus')
				->addRouteMatch('admin_user_edit')
				->addRouteMatch('admin_user_create')
				->addRouteMatch('admin_user_show')
		);

		$this->add(
			(new MenuContext('Bedrijven', 'admin_company_list'))
				->withIcon('fa-plus')
				->addRouteMatch('admin_company_edit')
				->addRouteMatch('admin_company_create')
				->addRouteMatch('admin_company_show')
		);

		$this->add(
			(new MenuContext('Pakketten', 'admin_package_list'))
				->withIcon('fa-plus')
				->addRouteMatch('admin_package_edit')
				->addRouteMatch('admin_package_create')
				->addRouteMatch('admin_package_show')
		);
	}

	/** @var MenuContext */
	protected $context = null;

	protected function add($context)
	{
		$this->begin($context);
		$this->end();
	}

	/**
	 * @param $context MenuContext
	 */
	protected function begin($context)
	{
		if($this->context)
		{
			$context->parameters = array_merge($this->context->parameters, $context->parameters);

			$context->item = $this->context->item->addChild($context->name, [
				'route' => $context->route,
				'routeParameters' => $context->parameters
			]);
		}else{
			$context->item = $this->factory->createItem($context->name, [
				'route' => $context->route,
				'routeParameters' => $context->parameters
			]);
		}

		$context->parent = $this->context;
		$this->context = $context;
	}

	protected function end()
	{
		$this->context->build();
		$result = $this->context->item;
		$this->context = $this->context->parent;

		return $result;
	}
}