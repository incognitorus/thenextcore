<?php
/**
 * User: Jos Craaijo
 * Date: 4-1-2016
 */

namespace TheNextSoftware\CoreBundle\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RequestVoter implements VoterInterface
{
	private $container;

	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function matchItem(ItemInterface $item)
	{
		$route =  $this->container->get('request_stack')->getMasterRequest()->get('_route');
		$requestUri = $this->container->get('request_stack')->getMasterRequest()->getRequestUri();

		if ($item->getUri() === $requestUri)
		{
			return true;
		} else if($item->getAttribute('match') !== null)
		{
			return preg_match($item->getAttribute('match'), $requestUri);
		} else if($item->getExtra('match-routes') !== null)
		{
			return in_array($route, $item->getExtra('match-routes'));
		}

		return null;
	}
}