# TheNextCore

Een verzameling van algemene classes, entities, en views voort TheNextReview, TheNextTable en TheNextWorkforce.

## Wat is een onderdeel van TheNextCore?

- Accounts, inclusief login, registratie, activatie, en wachtwoord-vergeten pagina's
- Restaurants, inclusief het aanmaken van een restaurant en kiezen van een pakket
- De menubalk en het thema
- Backaroo en Stripe integratie
- Medewerkers van een restaurant beheren
- Het admin panel voor restaurants, pakketten, promocodes en accounts

Extra thema's en functionaliteit worden door de individuele sites toegevoegd. In de app/Resources/TheNextCoreBundle
map van ieder project kunnen templates van TheNextCore worden overschreven met site-specifieke versies.
